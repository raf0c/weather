package com.example.raf0c.weather.view

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import com.example.raf0c.weather.model.CurrentWeather
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import java.text.SimpleDateFormat
import java.util.*
import android.support.v7.app.AlertDialog
import android.support.v4.content.ContextCompat
import android.location.Geocoder
import android.util.Log
import com.example.raf0c.weather.model.DaysAdapter
import com.example.raf0c.weather.R
import com.example.raf0c.weather.presenter.CurrentWeatherPresenter
import com.example.raf0c.weather.util.DateUtil
import com.example.raf0c.weather.util.toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), WeatherView {

	private val presenter: CurrentWeatherPresenter =
		CurrentWeatherPresenter()
	private lateinit var fusedLocationClient: FusedLocationProviderClient
	private var latitude: Double? = null
	private var longitud: Double? = null
	private var changeUnits: Boolean = false

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		setSupportActionBar(findViewById(R.id.main_screen_toolbar))
		fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
		checkLocationPermission()
		presenter.attachView(this)

		if (savedInstanceState != null) {
			latitude = savedInstanceState.getDouble(SAVE_INSTANCE_STATE_KEY_LATITUDE)
			longitud = savedInstanceState.getDouble(SAVE_INSTANCE_STATE_KEY_LONGITUDE)
			changeUnits = savedInstanceState.getBoolean(SAVE_INSTANCE_STATE_KEY_CHANGE_UNITS)
			presenter.fetchData(latitude, longitud, changeUnits)
		}

		swipe_to_refresh.setOnRefreshListener { presenter.fetchData(latitude, longitud, changeUnits) }
	}

	override fun onSaveInstanceState(outState: Bundle?) {
		super.onSaveInstanceState(outState)
		latitude?.let { outState?.putDouble(SAVE_INSTANCE_STATE_KEY_LATITUDE, it) }
		longitud?.let { outState?.putDouble(SAVE_INSTANCE_STATE_KEY_LONGITUDE, it) }
		outState?.putBoolean(SAVE_INSTANCE_STATE_KEY_CHANGE_UNITS, changeUnits)
	}

	override fun onResume() {
		super.onResume()
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
			PackageManager.PERMISSION_GRANTED
		) {

			fusedLocationClient.lastLocation
				.addOnSuccessListener { location: Location? ->
					latitude = location?.latitude
					longitud = location?.longitude
				}
		}
	}

	override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
		when (requestCode) {
			PERMISSION_ACCESS_FINE_LOCATION -> {
				if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

					if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
						fusedLocationClient.lastLocation
							.addOnSuccessListener { location: Location? ->
								latitude = location?.latitude
								longitud = location?.longitude
								presenter.fetchData(latitude, longitud, changeUnits)
							}
					}
				} else {
					toast("Please go to your settings and grant location services to use this app")
				}
			}
		}
	}

	override fun onCreateOptionsMenu(menu: Menu?): Boolean {
		val inflater: MenuInflater = menuInflater
		inflater.inflate(R.menu.main_menu, menu)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem?): Boolean {
		return when (item?.itemId) {
			R.id.help         -> {
				changeUnits = !changeUnits
				presenter.fetchData(latitude, longitud, changeUnits)
				true
			}
			R.id.menu_refresh -> {
				presenter.fetchData(latitude, longitud, changeUnits)
				true
			}
			else              -> {
				super.onOptionsItemSelected(item)
			}
		}
	}

	override fun onDestroy() {
		super.onDestroy()
		presenter.detachView()
	}

	override fun showWeather(currentWeather: CurrentWeather?) {

		try {
			val geo = Geocoder(this.applicationContext, Locale.getDefault())
			val addresses = geo.getFromLocation(latitude!!, longitud!!, 1)
			if (addresses.isEmpty()) {
				city.text = getString(R.string.main_screen_waiting_for_location)
			} else {
				if (addresses.size > 0) {
					city.text = addresses[0].locality
				}
			}
		} catch (e: Exception) {
			// getFromLocation() may sometimes fail
			Log.e(TAG, e.message, e)
		}

		currently.text = getString(R.string.main_screen_label_currently)
		updated.text = getString(R.string.main_screen_updated, DateUtil.convertTime(System.currentTimeMillis(), "h:mm a"))
		temperature.text = getString(R.string.temperature, currentWeather?.currently?.temperature.toString())
		feels_like.text =
				getString(R.string.main_screen_feels_like, currentWeather?.currently?.apparentTemperature.toString())
		days.adapter = DaysAdapter(currentWeather?.daily?.data ?: emptyList())
		swipe_to_refresh.isRefreshing = false;

	}

	private fun checkLocationPermission(): Boolean {
		if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			AlertDialog.Builder(this)
				.setTitle(getString(R.string.main_screen_warning_title))
				.setMessage(getString(R.string.main_screen_warning_body))
				.setPositiveButton(getString(R.string.main_screen_warning_positive_label)) { _, _ ->
					ActivityCompat.requestPermissions(
						this@MainActivity,
						arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
						PERMISSION_ACCESS_FINE_LOCATION
					)
				}
				.create()
				.show()

		} else {
			if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
				PackageManager.PERMISSION_GRANTED
			) {

				fusedLocationClient.lastLocation
					.addOnSuccessListener { location: Location? ->
						latitude = location?.latitude
						longitud = location?.longitude
						presenter.fetchData(latitude, longitud, changeUnits)
					}
			}
		}
		return true
	}

	companion object {
		const val TAG = "MainActivity"
		const val SAVE_INSTANCE_STATE_KEY_LATITUDE = "latitude"
		const val SAVE_INSTANCE_STATE_KEY_LONGITUDE = "longitude"
		const val SAVE_INSTANCE_STATE_KEY_CHANGE_UNITS = "changeUnits"

		const val PERMISSION_ACCESS_FINE_LOCATION = 99
	}
}
