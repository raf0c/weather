package com.example.raf0c.weather.model

data class Currently(
	val time: Long,
	val summary: String,
	val icon: String,
	val temperature: Double,
	val apparentTemperature: Double
)