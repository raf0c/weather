package com.example.raf0c.weather.model

data class Datum(
	var time: Long,
	val summary: String,
	val icon: String,
	val sunriseTime: Long,
	val sunsetTime: Long,
	val temperatureHigh: Double,
	val temperatureLow: Double,
	val apparentTemperatureHigh: Double,
	val apparentTemperatureLow: Double
)