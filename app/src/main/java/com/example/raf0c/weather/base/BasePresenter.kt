package com.example.raf0c.weather.base

abstract class BasePresenter<V> {

	protected var view: V? = null

	/**
	 * Called when we are ready to use the view
	 *
	 * @param view The view to be interacting with
	 * */
	fun attachView(view: V) {
		this.view = view
	}

	/**
	 * Called when the view has been destroyed
	 * */
	fun detachView() {
		view = null
	}

	/**
	 * Indicates if the view is available to use
	 * */
	fun isViewAttached() = view != null
}