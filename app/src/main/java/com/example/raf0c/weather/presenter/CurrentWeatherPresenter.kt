package com.example.raf0c.weather.presenter

import android.os.AsyncTask
import android.text.TextUtils
import android.util.Log
import com.example.raf0c.weather.ApiProvider
import com.example.raf0c.weather.base.BasePresenter
import com.example.raf0c.weather.view.WeatherView
import com.example.raf0c.weather.model.CurrentWeather
import com.example.raf0c.weather.model.Currently
import com.example.raf0c.weather.model.Daily
import com.example.raf0c.weather.model.Datum
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONObject
import java.io.IOException

class CurrentWeatherPresenter : BasePresenter<WeatherView>() {

	private val weatherApi = ApiProvider.weatherApiManager

	fun fetchData(latitude: Double?, longitude: Double?, toggleUnit: Boolean) {

		weatherApi.weatherService
			.getCurrentWeather(latitude, longitude, toggleUnit)
			.enqueue(object : Callback {

				override fun onResponse(call: Call, response: Response) {
					if (response.isSuccessful) {
						JsonParserTask(response.body()?.string() ?: "", view).execute()
					}
				}

				override fun onFailure(call: Call, e: IOException) {
					Log.e(TAG, e.message, e)
				}
			})

	}

	internal class JsonParserTask(
		private val jsonResponse: String,
		private val weatherView: WeatherView?
	) : AsyncTask<Void, Void, CurrentWeather>() {

		override fun doInBackground(vararg params: Void?): CurrentWeather? {

			if (TextUtils.isEmpty(jsonResponse)) return null

			val jsonObject = JSONObject(jsonResponse)

			val latitude = jsonObject.getDouble("latitude")
			val longitude = jsonObject.getDouble("longitude")
			val timezone = jsonObject.getString("timezone")
			val currentlyJsonObject = jsonObject.getJSONObject("currently")

			val currently = Currently(
				currentlyJsonObject.getLong("time"),
				currentlyJsonObject.getString("summary"),
				"",
				currentlyJsonObject.getDouble("temperature"),
				currentlyJsonObject.getDouble("apparentTemperature")
			)

			val dailyJsonObject = jsonObject.getJSONObject("daily")
			val dailySummary = dailyJsonObject.getString("summary")
			val dailyIcon = dailyJsonObject.getString("icon")
			val dailyDatumObject = dailyJsonObject.getJSONArray("data")

			var dailyDatum: Datum?
			val dailyList: MutableList<Datum> = mutableListOf()
			for (i in 0..(dailyDatumObject.length() - 1)) {
				val itemObject = dailyDatumObject.getJSONObject(i)
				val dailyTime = itemObject.getLong("time")
				val summary: String = itemObject.getString("summary")
				val icon: String = itemObject.getString("icon")
				val sunriseTime: Long = itemObject.getLong("sunriseTime")
				val sunsetTime: Long = itemObject.getLong("sunsetTime")
				val temperatureHigh: Double = itemObject.getDouble("temperatureHigh")
				val temperatureLow: Double = itemObject.getDouble("temperatureLow")
				val apparentTemperatureHigh: Double = itemObject.getDouble("apparentTemperatureHigh")
				val apparentTemperatureLow: Double = itemObject.getDouble("apparentTemperatureLow")

				dailyDatum = Datum(
					dailyTime,
					summary,
					icon,
					sunriseTime,
					sunsetTime,
					temperatureHigh,
					temperatureLow,
					apparentTemperatureHigh,
					apparentTemperatureLow
				)

				dailyList.add(dailyDatum)
			}

			val daily = Daily(dailySummary, dailyIcon, dailyList)


			return CurrentWeather(latitude, longitude, timezone, currently, daily)
		}

		override fun onPostExecute(result: CurrentWeather?) {
			super.onPostExecute(result)
			Log.e(TAG, "onPostExecute():: ${result?.toString()}")
			weatherView?.showWeather(result)
		}
	}

	companion object {
		const val TAG = "CurrentWeatherPresenter"
	}

}