package com.example.raf0c.weather

import android.app.Application
import com.example.raf0c.network.wapi.WeatherNetworkConfiguration

class WeatherApplication : Application() {

	override fun onCreate() {
		super.onCreate()
		ApiProvider.setConfiguration(WeatherNetworkConfiguration())
	}
}