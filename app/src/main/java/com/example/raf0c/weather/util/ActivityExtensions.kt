package com.example.raf0c.weather.util

import android.content.Context
import android.content.pm.PackageManager
import android.widget.Toast

fun Context.toast(message: CharSequence, duration: Int = Toast.LENGTH_LONG) {
	Toast.makeText(this, message, duration).show()
}