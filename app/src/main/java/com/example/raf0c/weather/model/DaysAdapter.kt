package com.example.raf0c.weather.model

import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.view.LayoutInflater
import android.widget.TextView
import com.example.raf0c.weather.R
import com.example.raf0c.weather.util.DateUtil

class DaysAdapter(private val items: List<Datum>) : BaseAdapter() {

	override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
		val view: View
		val viewHolder: ViewHolder

		if (convertView == null) {
			view = LayoutInflater.from(parent?.context).inflate(R.layout.item_day, parent, false)
			viewHolder = ViewHolder(view);
			view.tag = viewHolder
		} else {
			view = convertView
			viewHolder = view.tag as ViewHolder
		}

		// get current item to be displayed
		val currentItem = getItem(position) as Datum

		//since epoch is seconds and date takes milliseconds
		viewHolder.date.text = DateUtil.convertTime(currentItem.time * 1000, "EEE")
		viewHolder.valueMin.text = view.context.getString(R.string.temperature, currentItem.temperatureLow.toString())
		viewHolder.valueMax.text = view.context.getString(R.string.temperature, currentItem.temperatureHigh.toString())

		return view

	}

	override fun getItem(position: Int): Any = items[position]

	override fun getItemId(position: Int): Long = position.toLong()

	override fun getCount(): Int = items.size
}

class ViewHolder(private val view: View) {
	val date: TextView = view.findViewById(R.id.date)
	val valueMin: TextView = view.findViewById(R.id.value_min)
	val valueMax: TextView = view.findViewById(R.id.value_max)
}