package com.example.raf0c.weather.model

data class Daily(
	val summary: String,
	val icon: String,
	val data: List<Datum>
)