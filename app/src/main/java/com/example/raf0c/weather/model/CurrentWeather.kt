package com.example.raf0c.weather.model

data class CurrentWeather(
	val latitude: Double,
	val longitude: Double,
	val timezone: String,
	val currently: Currently,
	val daily: Daily
)