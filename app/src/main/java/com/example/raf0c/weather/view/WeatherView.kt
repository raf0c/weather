package com.example.raf0c.weather.view

import com.example.raf0c.weather.model.CurrentWeather


interface WeatherView {

	/**
	 * Method that indicates with the client view and determines
	 * show weather information
	 * */
	fun showWeather(currentWeather: CurrentWeather?)
}