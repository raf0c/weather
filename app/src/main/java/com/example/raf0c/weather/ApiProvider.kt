package com.example.raf0c.weather

import com.example.raf0c.network.common.NetworkConfiguration
import com.example.raf0c.network.wapi.WeatherApiManager

/**
 * This class, acts as a proxy for different apis that wants to be built,
 * Any "api_manager" created here, each can be configured specifically and be ready
 * for the client to use
 * */
object ApiProvider {

	lateinit var weatherApiManager: WeatherApiManager

	@JvmStatic
	fun setConfiguration(apiConfiguration: NetworkConfiguration) {
		weatherApiManager = WeatherApiManager(apiConfiguration)
	}
}