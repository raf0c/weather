package com.example.raf0c.weather.util

import java.text.SimpleDateFormat
import java.util.*

class DateUtil private constructor() {

	companion object {
		@JvmStatic fun convertTime(time: Long, pattern: String) : String {
			val date = Date(time)
			val format = SimpleDateFormat(pattern, Locale.US)
			return format.format(date)
		}
	}
}