package com.example.raf0c.network.wapi.services

import com.example.raf0c.network.wapi.WeatherNetworkConfiguration
import okhttp3.Call
import okhttp3.Request

class WeatherService(private val weatherConfig: WeatherNetworkConfiguration) : IWeatherService {

	override fun getCurrentWeather(latitude: Double?, longitude: Double?, toggleUnit: Boolean): Call {

		weatherConfig.path = "$latitude,$longitude"
		weatherConfig.changeUnits = toggleUnit
		weatherConfig.updateUnits()

		return weatherConfig.getOkHttpClient()
			.newCall(
				Request
					.Builder().url(weatherConfig.httpUrlBuilder(this::class.java).build())
					.build()
			)
	}


}

/**
 * Interface that creates different behaviours based on configurations set
 */
interface IWeatherService {

	/**
	 * Method that gets a behaviour call base on the configuration provided
	 *
	 * @param latitude the latitude object required for this behaviour
	 * @param longitude the longitude object required for this behaviour
	 * @param toggleUnit a boolean flag to change the units of the behaviour
	 * */
	fun getCurrentWeather(
		latitude: Double?,
		longitude: Double?,
		toggleUnit: Boolean
	): Call
}