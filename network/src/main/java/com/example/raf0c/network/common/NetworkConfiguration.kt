package com.example.raf0c.network.common

import okhttp3.HttpUrl
import okhttp3.OkHttpClient

/**
 * Base configuration for our network calls base on the current api design
 *
 * When using this, it will decouple from different "contexts" or behaviours,
 * such as WeatherApis, CatsApis, etc,
 * allowing abstraction of responsibilities and easiness to maintain
 * */
abstract class NetworkConfiguration {

	var path: String = ""

	abstract val queries: Map<String, String>

	abstract fun <T> getBaseUrl(clazz: T): String

	abstract fun <T> httpUrlBuilder(clazz: T): HttpUrl.Builder

	abstract fun getOkHttpClient(): OkHttpClient

}