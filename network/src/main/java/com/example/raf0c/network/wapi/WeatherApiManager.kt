package com.example.raf0c.network.wapi

import com.example.raf0c.network.NetworkServiceFactory
import com.example.raf0c.network.common.NetworkConfiguration
import com.example.raf0c.network.wapi.services.WeatherService

class WeatherApiManager(networkConfig: NetworkConfiguration) {

	/**
	 * Gets a weather service, created from the factory
	 * */
	val weatherService by lazy {
		NetworkServiceFactory.createServiceFor<WeatherService>(networkConfig)
	}
}