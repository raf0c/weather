package com.example.raf0c.network

import com.example.raf0c.network.common.NetworkConfiguration
import com.example.raf0c.network.wapi.WeatherNetworkConfiguration
import com.example.raf0c.network.wapi.services.WeatherService

/**
 * Class that provides NetworkServices based on ´a´ configuration
 * The configuration can be also extended for their better abstraction of responsibilities
 * */
class NetworkServiceFactory private constructor() {

	companion object {

		/**
		 * Method that receives a configuration provided from client wanted to construct a service
		 * Once executed it will return a Service object configured with client specs
		 *
		 * @param config the configuration object
		 * @return T A service of a type specified
		 * */
		@JvmStatic
		inline fun <reified T> createServiceFor(config: NetworkConfiguration): T {
			return when (T::class.java) {
				WeatherService::class.java -> WeatherService(config as WeatherNetworkConfiguration) as T
				else                       -> Any() as T
			}
		}

	}

}