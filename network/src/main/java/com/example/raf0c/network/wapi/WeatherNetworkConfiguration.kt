package com.example.raf0c.network.wapi

import com.example.raf0c.network.common.NetworkConfiguration
import com.example.raf0c.network.wapi.services.WeatherService
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

class WeatherNetworkConfiguration : NetworkConfiguration() {

	private val timeOut = 30000L

	var changeUnits: Boolean = false

	override val queries: HashMap<String, String> = hashMapOf(
		"exclude" to "hourly,flags",
		"units" to "si"
	)

	override fun <T> getBaseUrl(clazz: T): String = when (clazz) {
		WeatherService::class.java -> "$WEATHER_BASE_URL$path"
		else                       -> DEFAULT_BASE_URL
	}

	override fun <T> httpUrlBuilder(clazz: T): HttpUrl.Builder {
		val httpBuilder = HttpUrl.parse(getBaseUrl(clazz))?.newBuilder() ?: HttpUrl.Builder()
		for (query in queries.entries) {
			httpBuilder.addEncodedQueryParameter(query.key, query.value)
		}
		return httpBuilder
	}

	override fun getOkHttpClient(): OkHttpClient {
		val builder = OkHttpClient.Builder()

		builder.connectTimeout(timeOut, TimeUnit.MILLISECONDS)
		builder.readTimeout(timeOut, TimeUnit.MILLISECONDS)
		builder.writeTimeout(timeOut, TimeUnit.MILLISECONDS)

		return builder.build()
	}

	fun updateUnits() {
		for (query in queries.entries) {
			if ("units" == query.key) {
				query.setValue(if (changeUnits) "us" else "si")
			}
		}

	}

	companion object {
		const val WEATHER_BASE_URL = "https://api.darksky.net/forecast/2bb07c3bece89caf533ac9a5d23d8417/"
		const val DEFAULT_BASE_URL = "https://some.url"
	}

}